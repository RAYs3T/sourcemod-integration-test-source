FROM debian:stable-slim

MAINTAINER Kevin Urbainczyk <kevin@rays3t.info>

RUN apt-get update && \
    apt-get install -y curl wget lib32gcc1 lib32tinfo5

RUN useradd -m -s /bin/bash -U steam

USER steam

RUN mkdir -p ~/steamcmd &&\
    cd ~/steamcmd &&\
    curl -s http://media.steampowered.com/installer/steamcmd_linux.tar.gz | tar -vxz
    
RUN mkdir ~/server

WORKDIR ~/server

RUN ~/steamcmd/steamcmd.sh \
    +login anonymous \
    +force_install_dir ~/server \
    +app_update 232330 validate \
    +quit
    
RUN pwd

# Download / Install MM
# https://mms.alliedmods.net/mmsdrop/1.10/mmsource-latest-linux
RUN BASE_MM_DL_URL=https://mms.alliedmods.net/mmsdrop/1.10 && \
	LATEST_MM_VERSION=`wget $BASE_MM_DL_URL/mmsource-latest-linux -q -O -` && \
	echo Detected MetaMod version: $LATEST_MM_VERSION && \
    wget -qO ~/metamod.tar.gz $BASE_MM_DL_URL/$LATEST_MM_VERSION
    
RUN ls -all 
RUN tar xvzf ~/metamod.tar.gz -C ~/server/cstrike &&\
    rm ~/metamod.tar.gz

# Download SM / Install
RUN BASE_SM_DL_URL=http://www.sourcemod.net/smdrop/1.8 && \
	LATEST_SM_VERSION=`wget $BASE_SM_DL_URL/sourcemod-latest-linux -q -O -` && \
	echo Detected sourcemod version: $LATEST_SM_VERSION && \
    wget -qO ~/sourcemod.tar.gz $BASE_SM_DL_URL/$LATEST_SM_VERSION
    
RUN tar xvzf ~/sourcemod.tar.gz -C ~/server/cstrike &&\
    rm ~/sourcemod.tar.gz

RUN ls -all ~/server/cstrike/addons
